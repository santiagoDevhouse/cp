// config.js
module.exports = {
    TOKEN_SECRET: process.env.TOKEN_SECRET || "$2b$10$7XfRhByRhgXssF6ADn78Yz1.ssalhAiXmROWQ8Wb3Oyle4y"
  };