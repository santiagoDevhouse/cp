var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('./config');

exports.HasRole = function (roles) {
    return function (req, res, next) {
        if (!req.headers.authorization) {
            return res
                .status(403)
                .send({
                    message: "Tu petición no tiene cabecera de autorización"
                });
        }

        var token = req.headers.authorization.split(" ")[1];

        function isInArray(value, array) {
            return array.indexOf(value) > -1;
        }

        try {
            var payload = jwt.decode(token, config.TOKEN_SECRET);
            var decoded = jwt.decode(token, config.TOKEN_SECRET, false, 'HS256');
            //console.log(decoded.sub.roles[0].name);
            roles = roles.split(",");
            var userHasRole = false;
            decoded.sub.roles.forEach(function (role, index) {
                userHasRole = isInArray(role.name, roles);
                if(userHasRole){
                    return;
                }
            });
            
            if(!userHasRole){
                res
                .status(403)
                .send({
                    message: "No tienes permisos para esto "
                });
                return;
            }
            
        } catch (error) {
            res
                .status(403)
                .send({
                    message: "Usuario o clave incorrecta "
                });
            return;
        }

        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({
                    message: "El token ha expirado"
                });
        }


        req.user = payload.sub;
        next();
    }
}

exports.ensureAuthenticated = function (req, res, next) {

    if (!req.headers.authorization) {
        return res
            .status(403)
            .send({
                message: "Tu petición no tiene cabecera de autorización"
            });
    }

    var token = req.headers.authorization.split(" ")[1];

    try {
        var payload = jwt.decode(token, config.TOKEN_SECRET);
        //var decoded = jwt.decode(token, config.TOKEN_SECRET, false, 'HS256');
        //console.log(decoded.sub.roles);
    } catch (error) {
        res
            .status(403)
            .send({
                message: "Usuario o clave incorrecta"
            });
        return;
    }

    if (payload.exp <= moment().unix()) {
        return res
            .status(401)
            .send({
                message: "El token ha expirado"
            });
    }


    req.user = payload.sub;
    next();
}