var User = require('../../models/cr/user');
var bcrypt = require('bcrypt');
var path = require('path');
var fs = require('fs');


//publications of user

function savepublication(req, res) {

    var iduser = req.params.iduser;
    var publication = req.body;
    publication.status = "activo";
    publication.createdAt = Date.now();
    User.findOneAndUpdate({
            _id: iduser
        }, {
            $push: {
                publications: publication
            }
        },
        function (error, publicationStored) {
            if (error) {
                return res.status(500).send({
                    message: 'Error al agregar la publicación ' + error
                });
            } else {
                res.status(200).send({
                    publicationStored
                });
            }
        });
}

function updatePublication(req, res) {

    var iduser = req.params.iduser;
    var publicationId = req.params.publicationId;
    var publication = req.body;
    User.findOneAndUpdate({
            _id: iduser,
            "publications._id": publicationId
        }, {
            $set: {
                "publications.$.title": publication.title,
                "publications.$.description": publication.description
            },

        },
        function (error, publicationUpdated) {
            if (error) {
                res.status(500).send({
                    message: 'Error al agregar la publicación ' + error
                });
                return;
            }

            if (!publicationUpdated) {
                res.status(404).send({
                    message: 'La publicacion no existe'
                });
                return;
            }

            res.status(200).send({
                publicationUpdated
            });
        });
}

function getPublication(req, res) {
    var userId = req.params.userId;
    var publicationId = req.params.publicationId;

    User.findOne({
        _id: userId,
        "publications._id": publicationId
    }, {
        'publications.$': 1
    }, (err, user) => {
        if (err) {
            res.status(500).send({
                message: "Error en la peticion " + err
            });
            return;
        }

        if (!user) {
            res.status(404).send({
                message: "Publicacion no encontrada"
            });
            return;
        }

        res.status(200).send(user.publications[0]);

    });
}

function deletePublication(req, res) {
    var iduser = req.params.userId;
    var publicationId = req.params.publicationId;
    User.findOneAndUpdate({
            _id: iduser,
            "publications._id": publicationId
        }, {
            $pull: {
                "publications": {
                    "_id": publicationId
                }
            },

        },
        function (error, publicationDeleted) {
            if (error) {
                return res.status(500).send({
                    message: 'Error al agregar la publicación ' + error
                });
            }

            if (!publicationDeleted) {
                res.status(404).send({
                    message: 'La publicacion no existe'
                });

                return;
            }

            res.status(200).send({
                publicationDeleted
            });
        });
}

function uploadImage(req, res) {
    var publicationId = req.params.publicationId;
    var iduser = req.params.iduser;

    var fileName = 'No subido...';


    if (req.files) {

        var imageNames = [];
        var files = req.files.image;

        //first create the folder
        var target_path1 = './uploads/user-' + iduser;
        var target_path2 = './uploads/user-' + iduser + "/publication-" + publicationId;
        if (!fs.existsSync(target_path1)) {
            fs.mkdirSync(target_path1);
        }
        if (!fs.existsSync(target_path2)) {
            fs.mkdirSync(target_path2);
        }

        //delete all files first
        fs.readdir(target_path2, (err, files) => {
            if (err) {
                res.status(500).send(err);
                return;
            }

            for (const file of files) {
                fs.unlink(path.join(target_path2, file), err => {
                    if (err) {
                        res.status(500).send(err);
                        return;
                    }
                });
            }
        });


        files.forEach(function (file, index) {
            var filePath = file.path;
            var fileSplit = filePath.split('\\');
            var fileName = fileSplit[1];
            imageNames.push({
                name: fileName
            });

            //move the files
            var tmp_path = filePath;
            var target_path = target_path2 + "/" + fileName;




            fs.rename(tmp_path, target_path, function (err) {
                if (err) {
                    res.status(500).send(err);
                    return;
                }

                fs.unlink(tmp_path, function () {
                    if (err) {
                        res.status(500).send(err);
                        return;
                    }
                    //res.send('File uploaded to: ' + target_path);
                });
            });

        });

        User.findOneAndUpdate({
            _id: iduser,
            "publications._id": publicationId
        }, {
            $set: {
                "publications.$.pictures": imageNames
            }
        }, (err, imageUploaded) => {
            if (err) {
                return res.status(500).send({
                    message: 'Error al subir la imagen ' + err
                })
            }

            if (!imageUploaded) {
                return res.status(404).send({
                    message: "Imagen no subida "
                });
            }

            res.status(200).send({
                imageUploaded
            });
        });
    } else {
        res.status(200).send({
            message: 'No se ha subido imagen'
        });
    }
}


function getPublications(req, res) {

    User.find({}, (err, publications) => {
        if (err) {
            res.status(500).send({
                message: "Error en la peticion " + err
            });
            return;
        }

        if (!publications) {
            res.status(404).send({
                message: "Publicacion no encontrada"
            });
            return;
        }

        //res.status(200).send(publications[2].publications[0].title);
        res.status(200).send(publications);

    }).select("publications");
}


module.exports = {
    savepublication,
    getPublication,
    deletePublication,
    updatePublication,
    getPublications,
    uploadImage
}