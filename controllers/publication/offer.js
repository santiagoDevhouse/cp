var User = require('../../models/cr/user');
var bcrypt = require('bcrypt');
var path = require('path');
var fs = require('fs');


function addOffer(req, res) {

    var iduser = req.params.iduser;
    var publicationId = req.params.publicationId;

    var publicationOffer = req.body;
    publicationOffer.createdAt = Date.now();
    //return res.status(500).send(publicationOffer);
    User.findOneAndUpdate({
            _id: iduser,
            "publications._id": publicationId
        }, {
            $push: {
                "publications.$.publicationOffers": publicationOffer
            }
        },
        function (error, publicationOfferStored) {
            if (error) {
                return res.status(500).send({
                    message: 'Error al agregar la oferta ' + error
                });
            } else {
                res.status(200).send({
                    publicationOfferStored
                });
            }
        });

}

function updateOffer(req, res) {

    var owner = req.params.iduser;
    var publicationId = req.params.publicationId;
    var publicationOffer = req.body;


    User.findOne({
        _id: owner
    }, function (e, data) {
        if (e) {
            res.status(500).send({
                message: 'Error ' + e
            });
            return;
        }

        data.publications.id(publicationId).publicationOffers.id(publicationOffer._id).idpublication = publicationOffer.idpublication;

        data.save(function (error, publicationUpdated) {
            if (error) {
                res.status(500).send({
                    message: 'Error al actualizar la oferta publicación ' + error
                });
                return;
            }

            if (!publicationUpdated) {
                res.status(404).send({
                    message: 'La oferta publicacion no existe'
                });
                return;
            }

            res.status(200).send({
                publicationUpdated
            });
        });

    });
}

function getOffer(req, res) {

    var owner = req.params.iduser;
    var iduserInteresed = req.params.iduserInteresed;


    User.findOne({
        _id: owner,
        "publications.publicationOffers.iduser": iduserInteresed
    }, {
        'publications.$': 1
    }, (err, user) => {
        if (err) {
            res.status(500).send({
                message: "Error en la peticion " + err
            });
            return false;
        }

        if (!user) {
            res.status(404).send({
                message: "No se encontro el usuario"
            });
            return false;
        }

        //get just the only offer of user
        offerUser = [];
        var publicationPosts = user.publications[0].publicationOffers;
        publicationPosts.forEach(function (publicationPost, index) {
            if (publicationPost.iduser == iduserInteresed) {
                offerUser = publicationPost;
                return;
            }
        });

        return res.status(200).send(offerUser);

    });
}


module.exports = {
    addOffer,
    getOffer,
    updateOffer
}