'use strict'

var Categories = require('../../models/cr/category');


function getCategories(req, res){

    Categories.find({},(err, categories) =>{
        if(err){
            res.status(500).send({ message: 'Error en la peticion' });
            return;   
        }

        if(!categories){
            res.status(404).send({ message: 'No hay categories' });
            return
        }

        res.status(200).send({ categories });
    });
}


module.exports = {
    getCategories
}