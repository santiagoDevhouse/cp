'use strict'

var User = require('../../models/cr/user');
var bcrypt = require('bcrypt');
var service = require('../../security/services');

function login(req, res) {
    var params = req.body;


    User.findOne({
        email: params.email
    }, (err, user) => {
        if (err) {
            res.status(500).send({
                message: "Error en la peticion"
            });
            return;
        }

        if (!user) {
            res.status(404).send({
                message: "Usuario o clave incorrecta"
            });

            return;
        }


        bcrypt.compare(params.password, user.password, function (err, isPasswordCorrect) {
            if (!isPasswordCorrect) {
                res.status(404).send({
                    message: "Usuario o clave incorrecta"
                });
                return;
            }

            User.populate(user, {
                path: 'roles'
            }, (err, user) => {
                if (err) {
                    res.status(500).send({
                        message: "Error en la peticion"
                    });

                    return
                }

                res.status(200).send({
                    "user": user,
                    "token": service.createToken(user)
                });
            });

        });


    });
}



module.exports = {
    login
}