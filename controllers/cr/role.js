'use strict'

var Role = require('../../models/cr/role');


function getRoles(req, res){

    Role.find({},(err, roles) =>{
        if(err){
            res.status(500).send({ message: 'Error en la peticion' });
            return;   
        }

        if(!roles){
            res.status(404).send({ message: 'No hay roles' });
            return
        }

        res.status(200).send({ roles });
    });
}


module.exports = {
    getRoles
}