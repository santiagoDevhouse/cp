'use strict'

var User = require('../../models/cr/user');
var bcrypt = require('bcrypt');
var path = require('path');
var fs = require('fs');

function getUser(req, res) {
    var userId = req.params.id;

    User.findById(userId, (err, user) => {
        if (err) {
            res.status(500).send({
                message: "Error en la peticion"
            });
            return;
        }

        if (!user) {
            res.status(404).send({
                message: "User no encontrado"
            });
            return;
        }

        User.populate(user, {
            path: 'roles'
        }, (err, user) => {
            if (err) {
                res.status(500).send({
                    message: "Error en la peticion"
                });
                return;
            }

            res.status(200).send({
                user
            });
        });


    });
}

function getUsers(req, res) {

    var find = User.find({}).sort('-firstName');

    find.exec((err, users) => {
        if (err) {
            res.status(500).send({
                message: "Error en la peticion"
            });
            return;
        }

        if (!users) {
            res.status(404).send({
                message: "no hay users"
            });
            return;
        }

        User.populate(users, {
            path: 'publications.categories'
        }, (err, users) => {

            if (err) {
                res.status(500).send({
                    message: "Error en la peticion"
                });
                return;
            }

            if (!users) {
                res.status(404).send({
                    message: "User no encontrado"
                });
                return;
            }


            res.status(200).send({
                users
            });


            // var options = {
            //     path: 'publications.categories',
            //     select: 'subcategories.name'
            // };

            // if (err) {
            //     res.status(200).send({
            //         users
            //     });
            // }
            // User.populate(users, options, function (err, users) {
            //     res.status(200).send({
            //         users
            //     });
            // });

        });
    });
}


function saveUser(req, res) {
    var user = new User();
    var params = req.body;
    user.firstName = params.firstName;
    user.secondName = params.secondName;
    user.lastName = params.lastName;
    user.email = params.email;
    user.status = "activo";
    user.roles = params.roles;

    const saltRounds = 10;

    bcrypt.genSalt(saltRounds, function (err, salt) {
        bcrypt.hash(params.password, salt, function (err, hash) {
            user.password = hash;
            user.save((err, userStored) => {
                if (err) {
                    res.status(500).send({
                        message: 'Error al guardar ' + err
                    });
                    return;
                }

                if (!userStored) {
                    res.status(404).send({
                        message: 'No se ha guardado el user'
                    });
                    return;
                }

                res.status(200).send({
                    userStored
                });
            });
        });
    });


}


function updateUser(req, res) {
    var userId = req.params.id;
    var update = req.body;

    User.findByIdAndUpdate(userId, update, (err, userUpdated) => {
        if (err) {
            return res.status(500).send({
                message: 'Error al actualizar el user'
            });
        }

        if (!userUpdated) {
            return res.status(404).send({
                message: 'No se ha podido actualizar el user'
            });
        }

        return res.status(200).send({
            userUpdated
        });
    });
}


function deleteUser(req, res) {
    var albumId = req.params.id;

    User.findByIdAndRemove(albumId, (err, albumDeleted) => {
        if (err) {
            return res.status(500).send({
                message: 'Error al eliminar el album'
            });
        }

        if (!albumDeleted) {
            return res.status(404).send({
                message: 'No se ha podido eliminar el album'
            });
        }

        return res.status(200).send({
            albumDeleted
        });
    });
}



module.exports = {
    getUser,
    getUsers,
    saveUser,
    updateUser,
    deleteUser
}