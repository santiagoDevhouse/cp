'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

//carga de rutas
//cr
var roleRoutes = require('./routes/cr/role');
var userRoutes = require('./routes/cr/user');
var loginRoutes = require('./routes/cr/login');
//category
var categoryRoutes = require('./routes/cr/category');

//publication
var publicationRoutes = require('./routes/publication/publication');
var offerRoutes = require('./routes/publication/offer');



app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

//configurar cabeceras
app.use((req, res, next)=>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-With','Content-Type, Accept, Access-Control-Request-Method');
    res.header('Access-Control-Allow-Method', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

    next();
});


//rutas base
//cr
app.use('/api', roleRoutes);
app.use('/api', userRoutes);
app.use('/api', loginRoutes);
app.use('/api', categoryRoutes);

//publication
app.use('/api', publicationRoutes);
app.use('/api', offerRoutes);


module.exports = app;