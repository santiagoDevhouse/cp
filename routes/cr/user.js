'use strict'

var express = require('express');
var UserController = require('../../controllers/cr/user');
var api = express.Router();
var middlewareSecurity = require('../../security/middleware');

//api.get('/users/',middlewareSecurity.HasRole("admin,user"), UserController.getUsers );

//actions of user
api.get('/users/', UserController.getUsers);
api.get('/user/:id', UserController.getUser);
api.post('/user', UserController.saveUser);
api.put('/user/:id', UserController.updateUser);
api.delete('/user/:id', UserController.deleteUser);

module.exports = api;
