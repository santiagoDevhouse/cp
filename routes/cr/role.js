'use strict'

var express = require('express');
var RoleController = require('../../controllers/cr/role');
var api = express.Router();
var middlewareSecurity = require('../../security/middleware');

api.get('/roles', RoleController.getRoles );

module.exports = api;