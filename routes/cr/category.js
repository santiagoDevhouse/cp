'use strict'

var express = require('express');
var CategoryController = require('../../controllers/cr/category');
var api = express.Router();
var middlewareSecurity = require('../../security/middleware');

api.get('/categories', CategoryController.getCategories );

module.exports = api;