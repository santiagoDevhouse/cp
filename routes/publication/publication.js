'use strict'

var express = require('express');
var PublicationController = require('../../controllers/publication/publication');
var api = express.Router();
var middlewareSecurity = require('../../security/middleware');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart({
    uploadDir: './uploads'
});

//publications of user
api.get('/publication/:userId/:publicationId', PublicationController.getPublication);
api.get('/publications', PublicationController.getPublications);
api.delete('/publication/:userId/:publicationId', PublicationController.deletePublication);
api.post('/publication/:iduser', PublicationController.savepublication);
api.put('/publication/:iduser/:publicationId', PublicationController.updatePublication);
//images upload
api.post('/publication/uploadImage/:iduser/:publicationId', multipartMiddleware, PublicationController.uploadImage);


module.exports = api;
