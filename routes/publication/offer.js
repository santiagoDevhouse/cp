'use strict'

var express = require('express');
var OfferController = require('../../controllers/publication/offer');
var api = express.Router();

//api.get('/users/',middlewareSecurity.HasRole("admin,user"), UserController.getUsers );


api.post('/offer/:iduser/:publicationId', OfferController.addOffer);
api.get('/offer/:iduser/:publicationId/:iduserInteresed', OfferController.getOffer);
api.put('/offer/:iduser/:publicationId/:iduserInteresed', OfferController.updateOffer);

module.exports = api;
