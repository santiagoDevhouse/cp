const mongoose = require('mongoose');
const mongooseeder = require('mongooseeder');
const models = require('../models');
const mongodbUrl = 'mongodb://localhost:27017/changeProducts';


mongooseeder.seed({
    mongodbUrl: mongodbUrl,
    models: models,
    clean: true,
    mongoose: mongoose,
    seeds: () => {
        // Run your seeds here     
        // Example:     
        models.role.create([{
                name: 'admin',
                displayName: 'administrador'
            },
            {
                name: 'user',
                displayName: 'usuario'
            }
        ]);

        return models.category.create([{
                name: 'Hogar',
                subcategories: [{
                        name: 'Decoracion'
                    },
                    {
                        name: 'Electrodomésticos'
                    },
                    {
                        name: 'Cocina'
                    },
                    {
                        name: 'Muebles'
                    }
                ]
            },
            {
                name: 'Tecnología',
                subcategories: [{
                        name: 'Computador escritorio'
                    },
                    {
                        name: 'Laptop'
                    },
                    {
                        name: 'Celulares'
                    },
                    {
                        name: 'Tablets'
                    }
                ]
            }
        ]);

        return;
    }
});


// mongooseeder.clean({
//     mongodbUrl: mongodbUrl,
//     models: models,
//     mongoose: mongoose
// });

// mongooseeder.seed({
//     mongodbUrl: mongodbUrl,
//     models: models,
//     clean: false,
//     mongoose: mongoose,
//     seeds: () => {
//         // Run your seeds here     
//         // Example:     
//         return rolesSeeder.rolesSeeder;
//     }
// });