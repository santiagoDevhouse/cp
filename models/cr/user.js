'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UserSchema = Schema({
    firstName: {
        type: String,
        required: true
    },
    secondName: String,
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    roles: [{
        type: Schema.ObjectId,
        ref: 'Role'
    }],

    publications: [{
        title: {
            type: String,

        },
        description: String,
        status: {
            type: String,

        },
        createdAt: {
            type: Date
        },
        updatedAt: {
            type: Date,
            default: Date.now()
        },

        pictures: [{
            name: String
        }],

        categories: [{
            type: Schema.Types.ObjectId,
            refPath: 'Category'
        }],

        publicationOffers: [{
            idpublication: [{
                type: Schema.ObjectId,
                ref: 'User.publications'
            }],
            iduser: {
                type: Schema.ObjectId,
                ref: 'User'
            },
            createdAt: {
                type: Date
            }
        }]
    }]
});


module.exports = mongoose.model('User', UserSchema);