'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RoleSchema = Schema({
    name: String,
    displayName: String
});

module.exports = mongoose.model('Role',RoleSchema);